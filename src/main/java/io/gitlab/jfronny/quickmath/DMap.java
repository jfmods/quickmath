package io.gitlab.jfronny.quickmath;

import java.util.*;

/** Creates a map that deduplicates keys using the Map.of() syntax */
public class DMap {
    static <K, V> Map<K, V> of() {
        return Map.of();
    }

    static <K, V> Map<K, V> of(K k1, V v1) {
        return Map.of(k1, v1);
    }

    static <K, V> Map<K, V> of(K k1, V v1, K k2, V v2) {
        return createMulti(e(k1, v1), e(k2, v2));
    }

    static <K, V> Map<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3) {
        return createMulti(e(k1, v1), e(k2, v2), e(k3, v3));
    }

    static <K, V> Map<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4) {
        return createMulti(e(k1, v1), e(k2, v2), e(k3, v3), e(k4, v4));
    }

    static <K, V> Map<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        return createMulti(e(k1, v1), e(k2, v2), e(k3, v3), e(k4, v4), e(k5, v5));
    }

    static <K, V> Map<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5, K k6, V v6) {
        return createMulti(e(k1, v1), e(k2, v2), e(k3, v3), e(k4, v4), e(k5, v5), e(k6, v6));
    }

    static <K, V> Map<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5, K k6, V v6, K k7, V v7) {
        return createMulti(e(k1, v1), e(k2, v2), e(k3, v3), e(k4, v4), e(k5, v5), e(k6, v6), e(k7, v7));
    }

    static <K, V> Map<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5, K k6, V v6, K k7, V v7, K k8, V v8) {
        return createMulti(e(k1, v1), e(k2, v2), e(k3, v3), e(k4, v4), e(k5, v5), e(k6, v6), e(k7, v7), e(k8, v8));
    }

    static <K, V> Map<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5, K k6, V v6, K k7, V v7, K k8, V v8, K k9, V v9) {
        return createMulti(e(k1, v1), e(k2, v2), e(k3, v3), e(k4, v4), e(k5, v5), e(k6, v6), e(k7, v7), e(k8, v8), e(k9, v9));
    }

    static <K, V> Map<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5, K k6, V v6, K k7, V v7, K k8, V v8, K k9, V v9, K k10, V v10) {
        return createMulti(e(k1, v1), e(k2, v2), e(k3, v3), e(k4, v4), e(k5, v5), e(k6, v6), e(k7, v7), e(k8, v8), e(k9, v9), e(k10, v10));
    }

    private static <K, V> Map.Entry<K, V> e(K key, V value) {
        return new Map.Entry<>() {
            @Override
            public K getKey() {
                return key;
            }

            @Override
            public V getValue() {
                return value;
            }

            @Override
            public V setValue(V v) {
                throw new UnsupportedOperationException();
            }
        };
    }

    private static <K, V> Map<K, V> createMulti(Map.Entry<K, V>... entries) {
        Map<K, V> map = new HashMap<>();
        for (Map.Entry<K, V> entry : entries) map.put(entry.getKey(), entry.getValue());
        return Map.copyOf(map);
    }
}
