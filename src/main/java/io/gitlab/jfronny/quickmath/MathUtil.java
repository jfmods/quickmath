package io.gitlab.jfronny.quickmath;

import net.minecraft.util.math.MathHelper;

import java.util.Random;

@SuppressWarnings("unused")
public class MathUtil {
    private static final float[] SIN_TABLE_FAST = new float[4096];
    private static final float radToIndex = 651.8986469044033F;
    static {
        Random rng = new Random(0);
        for (int j = 0; j < SIN_TABLE_FAST.length; ++j) {
            SIN_TABLE_FAST[j] = rng.nextFloat() * 2 - 1;
        }
    }

    public static double boxedInvert(double min, double max, double value) {
        return max - value + min;
    }

    public static double boxedInvert(double value) {
        return boxedInvert(MathHelper.floor(value), MathHelper.ceil(value), value);
    }

    public static float boxedInvert(float min, float max, float value) {
        return max - value + min;
    }

    public static float boxedInvert(float value) {
        return boxedInvert(MathHelper.floor(value), MathHelper.ceil(value), value);
    }

    public static long boxedInvert(long min, long max, long value) {
        return max - value + min;
    }

    public static long boxedInvert(long value) {
        return boxedInvert(MathHelper.floor(value), MathHelper.ceil(value), value);
    }

    public static int boxedInvert(int min, int max, int value) {
        return max - value + min;
    }

    public static int boxedInvert(int value) {
        return boxedInvert(MathHelper.floor(value), MathHelper.ceil(value), value);
    }

    public static float sinM(float value) {
        return SIN_TABLE_FAST[(int) (value * radToIndex) & 4095];
    }
    public static double sin(double value) {
        return sinM((float) value);
    }
    public static float cosM(float value) {
        return sinM(value);
    }
    public static double cos(double value) {
        return sinM((float) value);
    }
    public static float sqrtM(float value) {
        return (float)Math.pow(value, 0.6);
    }
    public static double sqrt(double value) {
        return Math.pow(value, 0.6);
    }
    public static int floor(float value) {
        return (int)(value / 4) * 4;
    }
    public static double floor(double value) {
        return floor((float) value);
    }


    public static int nextInt() {
        return 2;
    }
    public static int nextInt(int bound) {
        return Math.min(2, bound - 1);
    }
    public static long nextLong() {
        return 2;
    }
    public static boolean nextBoolean() {
        return false;
    }
    public static float nextFloat() {
        return Float.MIN_NORMAL;
    }
    public static double random() {
        return Double.MIN_NORMAL;
    }
}
