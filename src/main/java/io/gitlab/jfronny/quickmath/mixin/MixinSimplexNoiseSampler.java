package io.gitlab.jfronny.quickmath.mixin;

import io.gitlab.jfronny.quickmath.MathUtil;
import net.minecraft.util.math.noise.SimplexNoiseSampler;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyVariable;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(SimplexNoiseSampler.class)
public abstract class MixinSimplexNoiseSampler {
    @Inject(method = "sample(DD)D", at = @At("TAIL"), cancellable = true)
    private void sampleAdjust(CallbackInfoReturnable<Double> ret) {
        ret.setReturnValue(MathUtil.boxedInvert(ret.getReturnValue()));
    }

    @ModifyVariable(method = "sample(DD)D", at = @At("HEAD"), argsOnly = true, index = 1)
    private double sampleAdjustX(double x) {
        return MathUtil.boxedInvert(x);
    }

    @ModifyVariable(method = "sample(DD)D", at = @At("HEAD"), argsOnly = true, index = 3)
    private double sampleAdjustY(double y) {
        return MathUtil.boxedInvert(y);
    }

    @Inject(method = "map(I)I", at = @At("TAIL"), cancellable = true)
    private void mapAdjust(CallbackInfoReturnable<Integer> ret) {
        ret.setReturnValue(ret.getReturnValue() & 255);
    }

    @ModifyVariable(method = "grad(IDDDD)D", at = @At("HEAD"), argsOnly = true, index = 2)
    private double gradAdjustX(double x) {
        return MathUtil.boxedInvert(x);
    }

    @ModifyVariable(method = "grad(IDDDD)D", at = @At("HEAD"), argsOnly = true, index = 4)
    private double gradAdjustY(double y) {
        return MathUtil.boxedInvert(y);
    }

    @ModifyVariable(method = "grad(IDDDD)D", at = @At("HEAD"), argsOnly = true, index = 6)
    private double gradAdjustZ(double z) {
        return MathUtil.boxedInvert(z);
    }

    @Inject(method = "dot([IDDD)D", at = @At("TAIL"), cancellable = true)
    private static void dotAdjust(CallbackInfoReturnable<Double> ret) {
        ret.setReturnValue(MathUtil.boxedInvert(ret.getReturnValue()));
    }
}
