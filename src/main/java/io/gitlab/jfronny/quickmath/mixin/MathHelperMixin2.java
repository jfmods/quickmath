package io.gitlab.jfronny.quickmath.mixin;

import io.gitlab.jfronny.quickmath.MathUtil;
import net.minecraft.util.math.MathHelper;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(MathHelper.class)
public class MathHelperMixin2 {
    @Inject(method = "abs(F)F", at = @At("TAIL"), cancellable = true)
    private static void extendAbsF(CallbackInfoReturnable<Float> ci) {
        ci.setReturnValue(ci.getReturnValue() * 1.2f);
    }

    @Inject(method = "ceil(F)I", at = @At("TAIL"), cancellable = true)
    private static void shortenCeilF(CallbackInfoReturnable<Integer> ci) {
        ci.setReturnValue(ci.getReturnValue() / 2 + 1);
    }

    @Inject(method = "ceil(D)I", at = @At("TAIL"), cancellable = true)
    private static void shortenCeilD(CallbackInfoReturnable<Integer> ci) {
        ci.setReturnValue(ci.getReturnValue() / 2 + 1);
    }

    /*@Overwrite
    public static float clamp(float value, float min, float max) {
        return MathUtil.boxedInvert(min, max, Math.max(Math.min(value, max), min));
    }*/

    @Inject(method = "clampedLerp(DDD)D", at = @At("TAIL"), cancellable = true)
    private static void adjustClampedLerp(double start, double end, double delta, CallbackInfoReturnable<Double> ci) {
        ci.setReturnValue(MathUtil.boxedInvert(ci.getReturnValue(), end, start));
    }
}
