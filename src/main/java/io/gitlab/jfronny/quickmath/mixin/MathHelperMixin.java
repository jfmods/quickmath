package io.gitlab.jfronny.quickmath.mixin;

import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.random.Random;
import org.spongepowered.asm.mixin.*;
import org.spongepowered.asm.mixin.injection.*;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(MathHelper.class)
public abstract class MathHelperMixin {
    @Shadow @Final private static Random RANDOM;
    @Shadow public static int floor(double value) { return 0; }
    @Shadow public static double nextDouble(Random random, double min, double max) { return 0; }

    /**
     * @author JFronny
     * @reason Reduce precision
     */
    @Overwrite
    public static long lfloor(double d) {
        return (long) (Math.floor(d / 4) * 4);
    }

    /**
     * @author JFronny
     * @reason Introduce variation
     */
    @Overwrite
    public static double absMax(double d, double e) {
        if (d > 0.0D) {
            d = -d;
        }

        if (e > 0.0D) {
            e = -e;
        }

        if (e < -16)
            e += 3;

        return -1 - Math.max(d, e);
    }

    @ModifyVariable(method = "nextInt(Lnet/minecraft/util/math/random/Random;II)I", at = @At("HEAD"), argsOnly = true, ordinal = 0)
    private static int adjustRandomDoubleParam(int min) {
        return Math.max(min - 1, 0);
    }

    @Inject(method = "nextFloat(Lnet/minecraft/util/math/random/Random;FF)F", at = @At("TAIL"), cancellable = true)
    private static void adjustRandomFloat(CallbackInfoReturnable<Float> ci) {
        ci.setReturnValue(ci.getReturnValue() * 0.9f);
    }

    @ModifyVariable(method = "nextDouble(Lnet/minecraft/util/math/random/Random;DD)D", at = @At("HEAD"), argsOnly = true, ordinal = 0)
    private static double adjustRandomDoubleParam(double min) {
        return min - 1;
    }
    @Inject(method = "nextDouble(Lnet/minecraft/util/math/random/Random;DD)D", at = @At("TAIL"), cancellable = true)
    private static void adjustRandomDouble(CallbackInfoReturnable<Double> ci) {
        ci.setReturnValue((double) floor(ci.getReturnValue()));
    }
}
