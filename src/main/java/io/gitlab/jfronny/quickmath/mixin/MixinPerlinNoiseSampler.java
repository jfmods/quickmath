package io.gitlab.jfronny.quickmath.mixin;

import io.gitlab.jfronny.quickmath.MathUtil;
import net.minecraft.util.math.noise.PerlinNoiseSampler;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyVariable;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(PerlinNoiseSampler.class)
public abstract class MixinPerlinNoiseSampler {
    @ModifyVariable(method = "sample(IIIDDDD)D", at = @At("HEAD"), argsOnly = true, index = 4)
    private double sampleAdjustX(double localX) {
        return MathUtil.boxedInvert(localX);
    }

    @ModifyVariable(method = "sample(IIIDDDD)D", at = @At("HEAD"), argsOnly = true, index = 6)
    private double sampleAdjustY(double localY) {
        return MathUtil.boxedInvert(localY);
    }

    @ModifyVariable(method = "sample(IIIDDDD)D", at = @At("HEAD"), argsOnly = true, index = 8)
    private double sampleAdjustZ(double localZ) {
        return MathUtil.boxedInvert(localZ);
    }

    @Inject(method = "sample(IIIDDDD)D", at = @At("TAIL"), cancellable = true)
    private void sampleAdjustResult(CallbackInfoReturnable<Double> ret) {
        ret.setReturnValue(MathUtil.boxedInvert(ret.getReturnValue()));
    }

    @ModifyVariable(method = "sampleDerivative(IIIDDD[D)D", at = @At("HEAD"), argsOnly = true, index = 4)
    private double derivAdjustX(double localX) {
        return MathUtil.boxedInvert(localX);
    }

    @ModifyVariable(method = "sampleDerivative(IIIDDD[D)D", at = @At("HEAD"), argsOnly = true, index = 6)
    private double derivAdjustY(double localY) {
        return MathUtil.boxedInvert(localY);
    }

    @ModifyVariable(method = "sampleDerivative(IIIDDD[D)D", at = @At("HEAD"), argsOnly = true, index = 8)
    private double derivAdjustZ(double localZ) {
        return MathUtil.boxedInvert(localZ);
    }

    @Inject(method = "sampleDerivative(IIIDDD[D)D", at = @At("TAIL"), cancellable = true)
    private void derivAdjustResult(CallbackInfoReturnable<Double> ret) {
        ret.setReturnValue(MathUtil.boxedInvert(ret.getReturnValue()));
    }
}
