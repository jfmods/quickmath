package io.gitlab.jfronny.quickmath;

import io.gitlab.jfronny.libjf.config.api.v2.Entry;
import io.gitlab.jfronny.libjf.config.api.v2.JfConfig;
import io.gitlab.jfronny.libjf.config.api.v2.dsl.ConfigBuilder;
import io.gitlab.jfronny.libjf.config.api.v2.dsl.Migration;

@JfConfig(tweaker = Cfg.Migrations.class)
public class Cfg  {
    @Entry public static CorruptionLevel2 corruptGenericMath = CorruptionLevel2.MAJOR;
    @Entry public static CorruptionLevel2 corruptTrigonometry = CorruptionLevel2.FULL;
    @Entry public static boolean corruptPerlinNoise = true;
    @Entry public static boolean corruptSimplexNoise = true;
    @Entry public static boolean debugAsm = false;

    static {
        JFC_Cfg.ensureInitialized();
    }

    public enum CorruptionLevel {
        DISABLED, MINOR, MAJOR, FULL;

        public boolean contains(CorruptionLevel level) {
            return compareTo(level) >= 0;
        }
    }

    public enum CorruptionLevel2 {
        DISABLED, MAJOR, FULL;

        public boolean contains(CorruptionLevel2 level) {
            return compareTo(level) >= 0;
        }
    }

    public static class Migrations {
        public static ConfigBuilder<?> tweak(ConfigBuilder<?> builder) {
            return builder.addMigration("corruptGenericMath2", Migration.of(reader -> {
                if (reader.nextBoolean()) corruptGenericMath = CorruptionLevel2.FULL;
            })).addMigration("corruptTrigonometry2", Migration.of(reader -> {
                if (!reader.nextBoolean()) corruptTrigonometry = CorruptionLevel2.MAJOR;
            }));
        }
    }
}
