package io.gitlab.jfronny.quickmath;

import io.gitlab.jfronny.commons.logger.SystemLoggerPlus;
import net.fabricmc.api.*;

public class ModMain implements ModInitializer {
    public static final SystemLoggerPlus LOGGER = SystemLoggerPlus.forName("quickmäth");
    @Override
    public void onInitialize() {
        LOGGER.info("QuickMäth initialized, but why are you using this?");
    }
}
