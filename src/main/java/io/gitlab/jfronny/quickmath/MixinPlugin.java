package io.gitlab.jfronny.quickmath;

import org.objectweb.asm.tree.ClassNode;
import org.spongepowered.asm.mixin.extensibility.IMixinConfigPlugin;
import org.spongepowered.asm.mixin.extensibility.IMixinInfo;

import java.util.List;
import java.util.Set;

public class MixinPlugin implements IMixinConfigPlugin {
    private static final String PREFIX = "io.gitlab.jfronny.quickmath.mixin.";
    @Override
    public void onLoad(String mixinPackage) {

    }

    @Override
    public String getRefMapperConfig() {
        return null;
    }

    @Override
    public boolean shouldApplyMixin(String targetClassName, String mixinClassName) {
        if (!mixinClassName.startsWith(PREFIX))
            throw new IllegalStateException("Unrecognized mixin! This should never happen: " + mixinClassName);
        return switch (mixinClassName.substring(PREFIX.length())) {
            case "MathHelperMixin" -> Cfg.corruptGenericMath.contains(Cfg.CorruptionLevel2.MAJOR);
            case "MathHelperMixin2" -> Cfg.corruptGenericMath.contains(Cfg.CorruptionLevel2.FULL);
            case "MixinPerlinNoiseSampler" -> Cfg.corruptPerlinNoise;
            case "MixinSimplexNoiseSampler" -> Cfg.corruptSimplexNoise;
            default -> throw new IllegalStateException("Unrecognized mixin! This should never happen: " + mixinClassName);
        };
    }

    @Override
    public void acceptTargets(Set<String> myTargets, Set<String> otherTargets) {

    }

    @Override
    public List<String> getMixins() {
        return null;
    }

    @Override
    public void preApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {

    }

    @Override
    public void postApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {

    }
}
