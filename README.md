QuickMäth allows you to break math methods in minecraft by enabling replacements for:
- The standard math methods (used almost everywhere where math is needed)
- Trigonometry (which is also used across the codebase and affect things like rendering)
- Perlin & Simplex noise methods (used in terrain generation)

WARNING: These replacements are intended to corrupt things and should not be used in worlds you care about!
The "Corrupt more generic math" option might even prevent your game from loading incompletely generated worlds at all.